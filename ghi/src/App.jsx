import React, { useState } from "react";
import "./App.css";
import { AuthProvider, useToken } from "@galvanize-inc/jwtdown-for-react";
import {
  BrowserRouter as Router,
  Route,
  Routes,
  Navigate,
} from "react-router-dom";

import Nav from "./Nav";
import LoginForm from "./Your-Space/LoginForm";
import SignupForm from "./Your-Space/SignupForm";
import TokenPage from "./Your-Space/TokenPage";
import BlogsList from "./Your-Space/BlogsList";
import BlogForm from "./Your-Space/BlogForm";

const baseUrl = "http://localhost:8000";

function App() {
  const [showLoginForm, setShowLoginForm] = useState(false);
  const [showSignupForm, setShowSignupForm] = useState(false);
  const [showSearchResults, setShowSearchResults] = useState(false);
  const [showTokenPage, setShowTokenPage] = useState(false);
  const [currentUserId, setCurrentUserId] = useState(null);
  return (
    <AuthProvider baseUrl={baseUrl}>
      <div id="root">
        <Router>
          <Nav
            setShowLoginForm={setShowLoginForm}
            setShowSignupForm={setShowSignupForm}
            setShowSearchResults={setShowSearchResults}
            setshowTokenPage={setShowTokenPage}
          />
          {showSearchResults && (
            <div className="search-results">Search Results</div>
          )}
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/login" element={<LoginForm />} />
            <Route path="/signup" element={<SignupForm />} />
            <Route
              path="/token"
              element={<TokenPage setCurrentUserId={setCurrentUserId} />}
            />
            <Route path="/blogs" element={<BlogsList />} />
            <Route path="/logout" element={<Logout />} />
            <Route
              path="/api/blogs"
              element={<BlogForm currentUserId={currentUserId} />}
            />
          </Routes>
        </Router>
      </div>
    </AuthProvider>
  );
}

function PrivateRoute({ element }) {
  const { isAuthenticated } = useToken();

  return isAuthenticated ? element : <Navigate to="/login" />;
}

function Logout() {
  const { logout } = useToken();
  logout();
  return <Navigate to="/login" />;
}

function Home() {
  return <h1>Welcome to Your Space Please Login in or Sign up!</h1>;
}

export default App;
