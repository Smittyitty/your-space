import React, { useState } from "react";
import "./SignupForm.css";
import LoginForm from "./LoginForm";

function SignupForm() {
  const [formData, setFormData] = useState({
    email: "",
    fullname: "",
    password: "",
    confirmPassword: "",
  });

  const [showLoginForm, setShowLoginForm] = useState(false);

  function handleChange(event) {
    const { name, value } = event.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  }

  const handleLoginClick = () => {
    setShowLoginForm(true);
  };

  async function handleSubmit(event) {
    event.preventDefault();

    const requestData = {
      email: formData.email,
      password: formData.password,
      fullname: formData.fullname,
    };

    try {
      const response = await fetch("http://localhost:8000/api/accounts", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(requestData),
      });

      if (response.ok) {
        setFormData({
          email: "",
          fullname: "",
          password: "",
          confirmPassword: "",
        });
      } else {
        console.error("Token request failed");
      }
    } catch (error) {
      console.error("Network error:", error);
    }
  }

  return (
    <div className="signup-form">
      <h2>Sign up</h2>
      <form onSubmit={handleSubmit}>
        <div>
          <input
            type="email"
            name="email"
            placeholder="Email"
            value={formData.email}
            onChange={handleChange}
            required
          />
        </div>
        <div>
          <input
            type="text"
            name="fullname"
            placeholder="Full Name"
            value={formData.fullname}
            onChange={handleChange}
            required
          />
        </div>
        <div>
          <input
            type="password"
            name="password"
            placeholder="Password"
            value={formData.password}
            onChange={handleChange}
            required
          />
        </div>
        <button className="nav-button" onClick={handleLoginClick}>
          Create Your Space
        </button>
      </form>

      {showLoginForm && <LoginForm />}
    </div>
  );
}

export default SignupForm;
