import useToken from "@galvanize-inc/jwtdown-for-react";
import { useNavigate } from "react-router-dom";
import React, { useState, useEffect } from "react";

function TokenPage({ setCurrentUserId }) {
  const { logout } = useToken();
  const [fullname, setUsername] = useState("");
  const [isLoading, setIsLoading] = useState(true);
  const navigate = useNavigate();
  const [data, setData] = useState(null);

  const fetchData = async () => {
    try {
      const url = "http://localhost:8000/token";
      const response = await fetch(url, {
        credentials: "include",
      });

      if (response.ok) {
        const data = await response.json();
        setData(data);

        if (data && data.account && data.account.id) {
          setCurrentUserId(data.account.id);
        }
        if (data && data.account && data.account.fullname) {
          setUsername(data.account.fullname);
        } else {
        }
      } else {
      }
    } catch (error) {
      console.error("An error occurred while fetching data:", error);
    } finally {
      setIsLoading(false);
    }
  };

  const handleGoToBlogs = () => {
    navigate("/blogs");
  };

  const handleLogout = () => {
    logout();
    navigate("/login");
  };

  useEffect(() => {
    const delay = setTimeout(() => {
      fetchData();
    }, 1500);

    return () => clearTimeout(delay);
  }, []);

  return (
    <div className="token-page">
      {isLoading ? (
        <h1>Loading...</h1>
      ) : fullname ? (
        <div>
          <h1>Hello, {fullname}</h1>
        </div>
      ) : (
        <h1>Please log in to see this page</h1>
      )}
    </div>
  );
}

export default TokenPage;
