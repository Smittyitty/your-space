import React, { useState } from "react";

const BlogForm = ({ currentUserId }) => {
  const [formData, setFormData] = useState({
    author: "",
    title: "",
    blog: "",
    comments: "",
    owner_id: null,
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const postData = {
      author: formData.author,
      title: formData.title,
      blog: formData.blog,
      comments: formData.comments,
      owner_id: currentUserId,
    };
    try {
      const response = await fetch("http://localhost:8000/api/blogs", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(postData),
      });

      if (response.ok) {
        const data = await response.json();
        setFormData({
          author: "",
          title: "",
          blog: "",
          comments: "",
          owner_id: null,
        });
      } else {
        console.error("Blog creation failed.");
      }
    } catch (error) {
      console.error("An error occurred while creating the blog:", error);
    }
  };

  return (
    <div className="blog-form">
      <h2>Create a Blog</h2>
      <form onSubmit={handleSubmit}>
        <div>
          <input
            type="text"
            name="author"
            placeholder="Author"
            value={formData.author}
            onChange={handleChange}
            required
          />
        </div>
        <div>
          <input
            type="text"
            name="title"
            placeholder="Title"
            value={formData.title}
            onChange={handleChange}
            required
          />
        </div>
        <div>
          <textarea
            name="blog"
            placeholder="Blog"
            value={formData.blog}
            onChange={handleChange}
            required
          />
        </div>
        <div>
          <input
            type="text"
            name="comments"
            placeholder="Comments"
            value={formData.comments}
            onChange={handleChange}
          />
        </div>
        <button type="submit">Create Blog</button>
      </form>
    </div>
  );
};

export default BlogForm;
