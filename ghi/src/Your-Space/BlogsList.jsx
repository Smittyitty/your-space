import React, { useState, useEffect } from "react";
import "./BlogsList.css";

const BlogsList = () => {
  const [blogs, setBlogs] = useState([]);

  useEffect(() => {
    fetch("http://localhost:8000/api/blogs")
      .then((response) => response.json())
      .then((data) => setBlogs(data.blogs))
      .catch((error) => console.error("Error fetching data:", error));
  }, []);

  return (
    <div className="blogs-list">
      <h2>Blogs List</h2>
      <ul>
        {blogs.map((blog) => (
          <li key={blog.id}>
            <h3>Title: {blog.title}</h3>
            <p>Blog Post: {blog.blog}</p>
            <p>Author: {blog.author}</p>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default BlogsList;
