import React, { useState } from "react";
import "./Nav.css";
import yourSpaceLogo from "./assets/your-space.png";
import Searchicon from "./assets/search-icon.png";
import SignupForm from "./Your-Space/SignupForm";
import LoginForm from "./Your-Space/LoginForm";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { NavLink, useNavigate } from "react-router-dom";

function Nav({
  setShowLoginForm,
  setShowSignupForm,
  setShowSearchResults,
  showSignupForm,
}) {
  const [searchQuery, setSearchQuery] = useState("");
  const { token, logout } = useToken();
  const navigate = useNavigate();

  const handleLogout = () => {
    logout();
    navigate("/login");
  };

  const handleLoginClick = () => {
    setShowLoginForm(true);
    setShowSignupForm(false);
    setShowSearchResults(false);
  };

  const handleSignupClick = () => {
    setShowSignupForm(true);
    setShowLoginForm(false);
    setShowSearchResults(false);
  };

  const handleSearchClick = () => {
    setShowSearchResults(true);
    setShowSignupForm(false);
    setShowLoginForm(false);
  };

  const handleLogoClick = () => {
    navigate("/");
  };

  return (
    <nav className="navs">
      <div className="nav-left">
        <NavLink to="/" className="nav-button">
          Home
        </NavLink>
        <div className="search-input-container">
          <input
            className="search-input"
            type="text"
            value={searchQuery}
            onChange={(e) => setSearchQuery(e.target.value)}
          />
          <button className="search-button" onClick={handleSearchClick}>
            <img src={Searchicon} className="search-icon" alt="Search" />
          </button>
        </div>
      </div>
      <img
        src={yourSpaceLogo}
        className="logo"
        alt="Your Space logo"
        onClick={handleLogoClick}
      />
      <div className="nav-right">
        {token ? (
          <>
            <NavLink to="/login" className="nav-button" onClick={handleLogout}>
              Logout
            </NavLink>
            <NavLink to="/blogs" className="nav-button">
              Blogs
            </NavLink>
            <NavLink to="/api/blogs" className="nav-button">
              Create a Blog
            </NavLink>
          </>
        ) : (
          <>
            <NavLink to="/login" className="nav-button">
              Login
            </NavLink>
            <NavLink to="/signup" className="nav-button">
              Signup
            </NavLink>
          </>
        )}
      </div>
      {showSignupForm && <SignupForm />}
    </nav>
  );
}

export default Nav;
