steps = [
    [
        """
        CREATE TABLE blogs (
            id SERIAL PRIMARY KEY NOT NULL,
            author VARCHAR(200) NOT NULL,
            title VARCHAR(200) NOT NULL,
            blog TEXT NOT NULL,
            comments TEXT,
            owner_id INT NOT NULL REFERENCES accounts("id")


        );
        """,
        """
        DROP TABLE blogs;
        """,
    ]
]
