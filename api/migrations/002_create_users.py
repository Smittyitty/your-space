steps = [
    [
        """
        CREATE TABLE users (
            id SERIAL PRIMARY KEY NOT NULL,
            first VARCHAR(200) NOT NULL,
            last VARCHAR(200) NOT NULL,
            avatar VARCHAR(200) NOT NULL,
            email VARCHAR(200) NOT NULL UNIQUE,
            username VARCHAR(200) NOT NULL
        );
        """,
        """
        DROP TABLE users;
        """,
    ]
]


