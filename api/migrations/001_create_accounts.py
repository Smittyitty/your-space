steps = [
    [
        """
        CREATE TABLE accounts (
            id SERIAL PRIMARY KEY NOT NULL,
            fullname VARCHAR(200) NOT NULL,
            email VARCHAR(200) NOT NULL UNIQUE,
            hashed_password VARCHAR(200) NOT NULL
        );
        """,
        """
        DROP TABLE accounts;
        """,
    ]
]
