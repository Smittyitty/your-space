from fastapi import FastAPI, APIRouter
from authenticator import authenticator
from fastapi.middleware.cors import CORSMiddleware
import os


from routers import users, blogs, accounts

app = FastAPI()

app.include_router(users.router)
app.include_router(blogs.router)
app.include_router(authenticator.router)
app.include_router(accounts.router)

app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        os.environ.get("CORS_HOST", "http://localhost:3000"),
        "http://localhost:3030",
        "http://localhost:8000",
        "http://localhost",
    ],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
