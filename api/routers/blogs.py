from typing import Literal
from fastapi import APIRouter, Depends, Response, HTTPException, status
from pydantic import BaseModel


from queries.db import BlogQueries

router = APIRouter()


class BlogIn(BaseModel):
    author: str
    title: str
    blog: str 
    comments: str
    owner_id: int


class BlogOut(BaseModel):
    id: int
    author: str
    title: str
    blog: str
    comments: str
    owner_id: int


class BlogsOut(BaseModel):
    blogs: list[BlogOut]

not_authorized = HTTPException(
    status_code=status.HTTP_401_UNAUTHORIZED,
    detail="Invalid authentication credentials",
    headers={"WWW-Authenticate": "Bearer"},
)

@router.get("/api/blogs/{blog_id}", response_model=BlogOut)
def get_blog(
    blog_id: int,
    queries: BlogQueries = Depends(),
):
    record = queries.get_blog(blog_id)
    if record is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Account not found")
    else:
        return BlogOut(**record)


@router.get("/api/blogs", response_model=BlogsOut)
def get_blogs(queries: BlogQueries = Depends()):
    return BlogsOut(blogs=queries.get_blogs())


@router.post("/api/blogs", response_model=BlogOut)
def create_blog(
    blog: BlogIn,
    queries: BlogQueries = Depends(),
):
    created_blog = queries.create_blog(blog)
    if created_blog is not None:
        return BlogOut(**created_blog)
    else:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Blog creation failed")


@router.delete("/api/blogs/{blog_id}", response_model=bool)
def delete_blog(blog_id: int, queries: BlogQueries = Depends()):
    queries.delete_blog(blog_id)
    return True