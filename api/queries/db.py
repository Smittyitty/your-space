import os
from psycopg_pool import ConnectionPool


pool = ConnectionPool(conninfo=os.environ["DATABASE_URL"])


class BlogQueries:
    def get_blogs(self):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT 
                        id,
                        author,
                        title, 
                        blog,
                        comments,
                        owner_id
                    FROM blogs 
                    """,
                )

                blogs = []
                rows = cur.fetchall()
                for row in rows:
                    blog = {
                        "id": row[0],
                        "author":row[1],
                        "title":row[2],
                        "blog":row[3],
                        "comments":row[4],
                        "owner_id":row[5],
                    }
                    blogs.append(blog)
                return blogs

    def get_blog(self, id:int):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT 
                        id,
                        author,
                        title, 
                        blog,
                        comments,
                        owner_id
                    FROM blogs 
                    WHERE id = %s
                    """,
                    [id],
                )

                row = cur.fetchone()
                blog = {
                    "id": row[0],
                    "author":row[1],
                    "title":row[2],
                    "blog":row[3],
                    "comments":row[4],
                    "owner_id":row[5],
                }
                return blog

    def delete_blog(self, blog_id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    DELETE FROM blogs
                    WHERE id = %s
                    """,
                    [blog_id],
                )


    def create_blog(self, blog):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    INSERT INTO blogs (
                        author, title, blog, comments, owner_id
                    )
                    VALUES (%s, %s, %s, %s, %s)
                    RETURNING id
                    """,
                    [
                        blog.author,
                        blog.title,
                        blog.blog,
                        blog.comments,
                        blog.owner_id,
                    ],
                )

                row = cur.fetchone()
                id = row[0]
        if id is not None:
            return self.get_blog(id)

    def blog_record_to_dict(self, row, description):
        blog = None
        if row is not None:
            blog = {}
            blog_fields = [
                "blog_id",
                "author",
                "title",
                "blog",
                "comments",
            ]
            for i, column in enumerate(description):
                if column.name in blog_fields:
                    blog[column.name] = row[i]
            blog["id"] = blog["blog_id"]

            owner = {}
            owner_fields = [
                "user_id",
                "first",
                "last",
                "avatar",
                "email",
                "username",
            ]
            for i, column in enumerate(description):
                if column.name in owner_fields:
                    owner[column.name] = row[i]
            owner["id"] = owner["user_id"]

            blog["owner"] = owner
        return blog


class UserQueries:
    def get_all_users(self):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT id, first, last, avatar,
                        email, username
                    FROM users
                    ORDER BY last, first
                """
                )

                results = []
                for row in cur.fetchall():
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
                    results.append(record)

                return results

    def get_user(self, id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT id, first, last, avatar,
                        email, username
                    FROM users
                    WHERE id = %s
                """,
                    [id],
                )

                record = None
                row = cur.fetchone()
                if row is not None:
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]

                return record

    def create_user(self, data):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                params = [
                    data.first,
                    data.last,
                    data.avatar,
                    data.email,
                    data.username,
                ]
                cur.execute(
                    """
                    INSERT INTO users (first, last, avatar, email, username)
                    VALUES (%s, %s, %s, %s, %s)
                    RETURNING id, first, last, avatar, email, username
                    """,
                    params,
                )

                record = None
                row = cur.fetchone()
                if row is not None:
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]

                return record

    def update_user(self, user_id, data):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                params = [
                    data.first,
                    data.last,
                    data.avatar,
                    data.email,
                    data.username,
                    user_id,
                ]
                cur.execute(
                    """
                    UPDATE users
                    SET first = %s
                    , last = %s
                    , avatar = %s
                    , email = %s
                    , username = %s
                    WHERE id = %s
                    RETURNING id, first, last, avatar, email, username
                    """,
                    params,
                )

                record = None
                row = cur.fetchone()
                if row is not None:
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]

                return record

    def delete_user(self, user_id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    DELETE FROM users
                    WHERE id = %s
                    """,
                    [user_id],
                )
